// Copyright Epic Games, Inc. All Rights Reserved.

#include "HoverVehicleGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, HoverVehicleGame, "HoverVehicleGame" );
