// Fill out your copyright notice in the Description page of Project Settings.


#include "HoverVehicle.h"
#include "Net/UnrealNetwork.h"

// Sets default values
AHoverVehicle::AHoverVehicle()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	VehicleMesh = CreateDefaultSubobject<UStaticMeshComponent>(FName("VehicleMesh"));
	VehicleMesh->SetupAttachment(RootComponent);

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(FName("SpringArm"));
	SpringArm->SetupAttachment(VehicleMesh);
	SpringArm->bUsePawnControlRotation = false;
	SpringArm->bInheritPitch = false;
	SpringArm->bInheritYaw = true;
	SpringArm->bInheritRoll = false;
	SpringArm->bDoCollisionTest = false;
	FVector SpringArmLocation = FVector(0.0f, 0.0f, 0.0f);
	SpringArm->SetRelativeLocation(SpringArmLocation);
	SpringArm->TargetArmLength = 1000 ;

	Camera = CreateDefaultSubobject<UCameraComponent>(FName("Camera"));
	Camera->SetupAttachment(SpringArm);

	bReplicates = true;


	VehicleMesh->SetSimulatePhysics(true);

	HoverComponentFL = CreateDefaultSubobject<UHoverVehicleComponent>(FName("HoveComponentFL"));
	HoverComponentFL->SetupAttachment(VehicleMesh);

	HoverComponentFR = CreateDefaultSubobject<UHoverVehicleComponent>(FName("HoveComponentFR"));
	HoverComponentFR->SetupAttachment(VehicleMesh);

	HoverComponentRL = CreateDefaultSubobject<UHoverVehicleComponent>(FName("HoveComponentRL"));
	HoverComponentRL->SetupAttachment(VehicleMesh);

	HoverComponentRR = CreateDefaultSubobject<UHoverVehicleComponent>(FName("HoveComponentRR"));
	HoverComponentRR->SetupAttachment(VehicleMesh);

	TrailSpawnPoint = CreateDefaultSubobject<USceneComponent>(FName("TrailSpawnPoint"));
	TrailSpawnPoint->SetupAttachment(VehicleMesh);

}

// Called when the game starts or when spawned
void AHoverVehicle::BeginPlay()
{
	Super::BeginPlay();

	HoverComponentFL->SetParentComponent(VehicleMesh);
	HoverComponentFR->SetParentComponent(VehicleMesh);
	HoverComponentRL->SetParentComponent(VehicleMesh);
	HoverComponentRR->SetParentComponent(VehicleMesh);

}


void AHoverVehicle::GetLifetimeReplicatedProps(TArray< FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	//DOREPLIFETIME(AHoverVehicle, HoverComponentFL);
	//DOREPLIFETIME(AHoverVehicle, HoverComponentFR);
	//DOREPLIFETIME(AHoverVehicle, HoverComponentRL);
	//DOREPLIFETIME(AHoverVehicle, HoverComponentRR);
	DOREPLIFETIME(AHoverVehicle, Trail);
	DOREPLIFETIME(AHoverVehicle, VehicleMesh);
	//DOREPLIFETIME(AHoverVehicle, ForwardForceToAdd);
	//DOREPLIFETIME(AHoverVehicle, RotationForceToAdd);
	//DOREPLIFETIME(AHoverVehicle, ForwardForce);
	//DOREPLIFETIME(AHoverVehicle, RotationForce);
}

// Called every frame
void AHoverVehicle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);	
}

// Called to bind functionality to input
void AHoverVehicle::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("LookUp", this, &AHoverVehicle::Client_LookUp);
	PlayerInputComponent->BindAxis("LookRight", this, &AHoverVehicle::Client_LookRight);
	PlayerInputComponent->BindAxis("Forward", this, &AHoverVehicle::Server_MoveForward);
	PlayerInputComponent->BindAxis("Right", this, &AHoverVehicle::Server_TurnRight);
	PlayerInputComponent->BindAxis("RollRight", this, &AHoverVehicle::Server_RollRight);
	PlayerInputComponent->BindAxis("PitchUp", this, &AHoverVehicle::Server_PitchUp);
	PlayerInputComponent->BindAction("AddTrail", EInputEvent::IE_Pressed, this, &AHoverVehicle::Server_AddTrailPressed);
	PlayerInputComponent->BindAction("AddTrail", EInputEvent::IE_Released, this, &AHoverVehicle::Server_AddTrailReleased);
}

void AHoverVehicle::Client_LookUp(float Axis)
{
	SpringArm->AddRelativeRotation(FRotator(Axis, 0, 0));
	//CameraRotation = SpringArm->GetComponentRotation();
}

void AHoverVehicle::Client_LookRight(float Axis)
{
	SpringArm->AddRelativeRotation(FRotator(0, Axis, 0));
	//CameraRotation = SpringArm->GetComponentRotation();
}


void AHoverVehicle::Server_MoveForward_Implementation(float Axis)
{

	/*if (GEngine && Axis != 0)
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, TEXT("I AM NOT AUTHORITE"));*/
		//UE_LOG(LogTemp, Warning, TEXT("TRANSFORM, %f %f %f"), ForwardForce.X, ForwardForce.Y, ForwardForce.Z);

	float forceToAdd = ForwardForceToAdd * Axis;
	FVector ForwardVector = VehicleMesh->GetForwardVector();
	ForwardForce = ForwardVector * forceToAdd;
	if (Axis > 0) {
		HoverComponentRL->GetParentComponent()->AddForceAtLocation(ForwardForce, HoverComponentRL->GetComponentLocation(), NAME_None);
		HoverComponentRR->GetParentComponent()->AddForceAtLocation(ForwardForce, HoverComponentRR->GetComponentLocation(), NAME_None);
	}
	else if (Axis < 0)
	{
		HoverComponentFL->GetParentComponent()->AddForceAtLocation(ForwardForce, HoverComponentFL->GetComponentLocation(), NAME_None);
		HoverComponentFR->GetParentComponent()->AddForceAtLocation(ForwardForce, HoverComponentFR->GetComponentLocation(), NAME_None);
	}
}

bool AHoverVehicle::Server_MoveForward_Validate(float Axis)
{
	return true;
}

void AHoverVehicle::Server_TurnRight_Implementation(float Axis)
{
	float forceToAdd = RotationForceToAdd * Axis;
	FVector RightVector = VehicleMesh->GetRightVector();
	RotationForce = RightVector * forceToAdd;

	if (Axis > 0) {
		HoverComponentFL->GetParentComponent()->AddForceAtLocation(RotationForce, HoverComponentFL->GetComponentLocation(), NAME_None);
		HoverComponentRR->GetParentComponent()->AddForceAtLocation(RotationForce * -1, HoverComponentRR->GetComponentLocation(), NAME_None);
	}
	else if (Axis < 0)
	{
		HoverComponentFR->GetParentComponent()->AddForceAtLocation(RotationForce, HoverComponentFR->GetComponentLocation(), NAME_None);
		HoverComponentRL->GetParentComponent()->AddForceAtLocation(RotationForce * -1, HoverComponentRL->GetComponentLocation(), NAME_None);
	}
}

bool AHoverVehicle::Server_TurnRight_Validate(float Axis)
{
	return true;
}

void AHoverVehicle::Server_RollRight_Implementation(float Axis)
{
	float forceToAdd = RollForceToAdd * Axis;
	FVector UpVector = VehicleMesh->GetUpVector();
	RollForce = UpVector * forceToAdd;

	//if (Axis > 0) {
	HoverComponentFL->GetParentComponent()->AddForceAtLocation(RollForce, HoverComponentFL->GetComponentLocation(), NAME_None);
	HoverComponentRL->GetParentComponent()->AddForceAtLocation(RollForce, HoverComponentRL->GetComponentLocation(), NAME_None);
	HoverComponentFR->GetParentComponent()->AddForceAtLocation(RollForce * -1, HoverComponentFR->GetComponentLocation(), NAME_None);
	HoverComponentRR->GetParentComponent()->AddForceAtLocation(RollForce * -1, HoverComponentRR->GetComponentLocation(), NAME_None);
	//}
	/*else if (Axis < 0)
	{
		HoverComponentFL->GetParentComponent()->AddForceAtLocation(RollForce, HoverComponentFL->GetComponentLocation(), NAME_None);
		HoverComponentRL->GetParentComponent()->AddForceAtLocation(RollForce, HoverComponentRL->GetComponentLocation(), NAME_None);
		HoverComponentFR->GetParentComponent()->AddForceAtLocation(RollForce * -1, HoverComponentFR->GetComponentLocation(), NAME_None);
		HoverComponentRR->GetParentComponent()->AddForceAtLocation(RollForce * -1, HoverComponentRR->GetComponentLocation(), NAME_None);
	}*/
}

bool AHoverVehicle::Server_RollRight_Validate(float Axis)
{
	return true;
}

void AHoverVehicle::Server_PitchUp_Implementation(float Axis)
{
	float forceToAdd = PitchForceToAdd * Axis;
	FVector UpVector = VehicleMesh->GetUpVector();
	PitchForce = UpVector * forceToAdd;

	HoverComponentFL->GetParentComponent()->AddForceAtLocation(PitchForce, HoverComponentFL->GetComponentLocation(), NAME_None);
	HoverComponentFR->GetParentComponent()->AddForceAtLocation(PitchForce, HoverComponentFR->GetComponentLocation(), NAME_None);
	HoverComponentRL->GetParentComponent()->AddForceAtLocation(PitchForce * -1, HoverComponentRL->GetComponentLocation(), NAME_None);
	HoverComponentRR->GetParentComponent()->AddForceAtLocation(PitchForce * -1, HoverComponentRR->GetComponentLocation(), NAME_None);

	
	if (FMath::Abs(Axis) > 0 || FMath::Abs(Axis) < 0) {
		//SpringArm->SetWorldRotation(CameraRotation);
		SpringArm->CameraRotationLagSpeed = 0.25;
		SpringArm->bInheritYaw = true;
	}
	else {
		//SpringArm->SetWorldRotation(CameraRotation);
		SpringArm->CameraRotationLagSpeed = 5;
		SpringArm->bInheritYaw = true;
	}
}

bool AHoverVehicle::Server_PitchUp_Validate(float Axis)
{
	return true;
}

void AHoverVehicle::Server_AddTrailPressed_Implementation()
{
	FActorSpawnParameters SpawnInfo;
	if (IsValid(Trail))
	{
		SpawnedTrail = GetWorld()->SpawnActor<ATrail>(Trail, TrailSpawnPoint->GetComponentLocation(), TrailSpawnPoint->GetComponentRotation(), SpawnInfo);
		SpawnedTrail->SetTrailSpawn(TrailSpawnPoint);
		SpawnedTrail->setShouldGrow(true);
	}
}

bool AHoverVehicle::Server_AddTrailPressed_Validate()
{
	return true;
}

void AHoverVehicle::Server_AddTrailReleased_Implementation()
{
	if (IsValid(Trail))
	{
		SpawnedTrail->setShouldGrow(false);
	}
}

bool AHoverVehicle::Server_AddTrailReleased_Validate()
{
	return true;
}