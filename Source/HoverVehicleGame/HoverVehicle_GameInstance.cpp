// Fill out your copyright notice in the Description page of Project Settings.


#include "HoverVehicle_GameInstance.h"
#include "Kismet/GameplayStatics.h"
#include "UObject/ConstructorHelpers.h"
#include "Blueprint/UserWidget.h"
#include "Engine/World.h"

const static FName SESSION_NAME = TEXT("HoverVehicleSession");
const static FName SERVER_NAME_SETTINGS_KEY = TEXT("HoverVehicleGameServer");

UHoverVehicle_GameInstance::UHoverVehicle_GameInstance()
{
	ConstructorHelpers::FClassFinder<UUserWidget> MainMenuBPClass(TEXT("/Game/Maps/Menu/WB_Main_Menu"));
	if (!ensure(MainMenuBPClass.Class != nullptr)) return;

	MainMenuClass = MainMenuBPClass.Class;
}

void UHoverVehicle_GameInstance::Init()
{
	Super::Init();

	if (IOnlineSubsystem* Subsystem = IOnlineSubsystem::Get())
	{
		SessionInterface = Subsystem->GetSessionInterface();
		if (SessionInterface.IsValid())
		{
			SessionInterface->OnCreateSessionCompleteDelegates.AddUObject(this, &UHoverVehicle_GameInstance::OnCreateSessionComplete);
			SessionInterface->OnDestroySessionCompleteDelegates.AddUObject(this, &UHoverVehicle_GameInstance::OnDestroySessionComplete);
			SessionInterface->OnFindSessionsCompleteDelegates.AddUObject(this, &UHoverVehicle_GameInstance::OnFindSessionComplete);
			SessionInterface->OnJoinSessionCompleteDelegates.AddUObject(this, &UHoverVehicle_GameInstance::OnJoinSessionComplete);
		}
	}
}

void UHoverVehicle_GameInstance::LoadMenu()
{
	if (!ensure(MainMenuClass.Get() != nullptr)) return;

	UUserWidget* Menu = CreateWidget<UUserWidget>(this, MainMenuClass);
	if (!ensure(Menu != nullptr)) return;
	Menu->AddToViewport();
	Menu->bIsFocusable = true;
	UGameplayStatics::GetPlayerController(GetWorld(), 0)->bShowMouseCursor = true;
}

void UHoverVehicle_GameInstance::OnCreateSessionComplete(FName SessionName, bool Succeeded)
{
	if (Succeeded)
	{
		GetWorld()->ServerTravel("/Game/Maps/Lobby/Lobby?listen");
	}
}

void UHoverVehicle_GameInstance::OnDestroySessionComplete(FName SessionName, bool Succeeded)
{
	/*if (Succeeded)
	{
		CreateSession(SessionName);
	}*/
}

void UHoverVehicle_GameInstance::OnFindSessionComplete(bool Succeeded)
{
	if (!Succeeded) {
		UE_LOG(LogTemp, Warning, TEXT("OnFindSessionComplete, FAILED"));
	}
	if (Succeeded && SessionSearch.IsValid())
	{
		UE_LOG(LogTemp, Warning, TEXT("OnFindSessionComplete, SUCCESS"));
		int SearchIndex = 0;
		for (const FOnlineSessionSearchResult& SearchResult : SessionSearch->SearchResults)
		{
			UE_LOG(LogTemp, Warning, TEXT("Found session names: %s"), *SearchResult.GetSessionIdStr());
			FServerData Data;
			Data.MaxPlayers = SearchResult.Session.SessionSettings.NumPublicConnections;
			Data.CurrentPlayers = Data.MaxPlayers - SearchResult.Session.NumOpenPublicConnections;
			Data.HostUsername = SearchResult.Session.OwningUserName;
			if (SearchResult.Session.SessionSettings.Get(SERVER_NAME_SETTINGS_KEY, ServerName))
			{
				Data.Name = ServerName;
			}
			else
			{
				Data.Name = "Could not find name.";
			}
			Data.Index = SearchIndex;
			ServerList.Add(Data);
			SearchIndex++;
		}
		//SessionInterface->JoinSession(0, FName("Hover Session"), SearchResults[0]);

		/*if (SearchResults.Num())
		{
			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Green, FString::Printf(TEXT("FOUND")));
			SessionInterface->JoinSession(0, FName("Hover Session"), SearchResults[0]);
		}
		else {
			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, FString::Printf(TEXT("NO SESSION FOUND")));
			UE_LOG(LogTemp, Warning, TEXT("NO SESSION FOUND"));
		}*/
	}
}

void UHoverVehicle_GameInstance::OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result)
{
	if (APlayerController* PController = UGameplayStatics::GetPlayerController(GetWorld(), 0))
	{
		UE_LOG(LogTemp, Warning, TEXT("PLAYER CONTROLLER FOUND"));
		FString JoinAddress = "";
		SessionInterface->GetResolvedConnectString(SessionName, JoinAddress);
		if (JoinAddress != "")
		{
			UE_LOG(LogTemp, Warning, TEXT("Join Address FOUND %s"), *JoinAddress);
			PController->ClientTravel(JoinAddress, ETravelType::TRAVEL_Absolute);
		}
		else {
			UE_LOG(LogTemp, Error, TEXT("Join Address NOT FOUND"));
		}
	}
}

void UHoverVehicle_GameInstance::CreateSession(FString DesiredServerName)
{
	UE_LOG(LogTemp, Warning, TEXT("Create Session"));

	FOnlineSessionSettings SessionSettings;
	SessionSettings.bIsLANMatch = (IOnlineSubsystem::Get()->GetSubsystemName() == "NULL");
	SessionSettings.NumPublicConnections = 10;
	SessionSettings.bShouldAdvertise = true;
	SessionSettings.bUsesPresence = true;
	SessionSettings.Set(SERVER_NAME_SETTINGS_KEY, DesiredServerName, EOnlineDataAdvertisementType::ViaOnlineServiceAndPing);

	auto ExistingSession = SessionInterface->GetNamedSession(SESSION_NAME);
	if (ExistingSession == nullptr)
	{
		SessionInterface->CreateSession(0, SESSION_NAME, SessionSettings);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Session Exists. Destroying now."));
		SessionInterface->DestroySession(SESSION_NAME);
	}
}

void UHoverVehicle_GameInstance::FindServers()
{
	UE_LOG(LogTemp, Warning, TEXT("JOINING SERVER"));

	SessionSearch = MakeShareable(new FOnlineSessionSearch());
	SessionSearch->bIsLanQuery = (IOnlineSubsystem::Get()->GetSubsystemName() == "NULL");
	SessionSearch->MaxSearchResults = 100;
	SessionSearch->QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);

	SessionInterface->FindSessions(0, SessionSearch.ToSharedRef());
}

void UHoverVehicle_GameInstance::JoinServer()
{
	if (SelectedServer.CurrentPlayers > 0 &&(SelectedServer.CurrentPlayers < SelectedServer.MaxPlayers))
	{
		SessionInterface->JoinSession(0, SESSION_NAME, SessionSearch->SearchResults[SelectedServer.Index]);
	}
}

void UHoverVehicle_GameInstance::SelectServer(FServerData ServerToSelect)
{
	SelectedServer = ServerToSelect;
}