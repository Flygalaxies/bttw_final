// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "OnlineSubsystem.h"
#include "OnlineSessionSettings.h"
#include "Interfaces/OnlineSessionInterface.h"
#include "HoverVehicle_GameInstance.generated.h"


USTRUCT(BlueprintType)
struct HOVERVEHICLEGAME_API FServerData
{
	GENERATED_USTRUCT_BODY()

public:

	UPROPERTY(Category = ServerData, BlueprintReadWrite, VisibleAnywhere)
		FString Name;
	UPROPERTY(Category = ServerData, BlueprintReadWrite, VisibleAnywhere)
		int32  CurrentPlayers;
	UPROPERTY(Category = ServerData, BlueprintReadWrite, VisibleAnywhere)
		int32 MaxPlayers;
	UPROPERTY(Category = ServerData, BlueprintReadWrite, VisibleAnywhere)
		FString HostUsername;
	UPROPERTY(Category = ServerData, BlueprintReadWrite, VisibleAnywhere)
		int32 Index;
};
/**
 *
 */
UCLASS()
class HOVERVEHICLEGAME_API UHoverVehicle_GameInstance : public UGameInstance
{
	GENERATED_BODY()

public:

	UHoverVehicle_GameInstance();

	UFUNCTION(BlueprintCallable)
		void LoadMenu();


protected:

	TSharedPtr<FOnlineSessionSearch> SessionSearch;
	IOnlineSessionPtr  SessionInterface;

	virtual void Init() override;

	virtual void OnCreateSessionComplete(FName SessionName, bool Succeeded);
	virtual void OnDestroySessionComplete(FName SessionName, bool Succeeded);
	virtual void OnFindSessionComplete(bool Succeeded);
	virtual void OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result);

	UFUNCTION(BlueprintCallable)
		void CreateSession(FString DesiredServerName);

	UFUNCTION(BlueprintCallable)
		void JoinServer();

	UFUNCTION(BlueprintCallable)
		void FindServers();

	UFUNCTION(BlueprintCallable)
		void SelectServer(FServerData ServerToSelect);

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
		TArray<FServerData> ServerList;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
		FServerData ServerData;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
		FServerData SelectedServer;



private:
	FString ServerName;
	TSubclassOf<class UUserWidget> MainMenuClass;

};
